package com.example.spendlist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class SignUp extends AppCompatActivity {

    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        btn = findViewById(R.id.JoinUs);
    }

    public void JoinUsClick(View view) {

        Intent dsp = new Intent(SignUp.this,LogIn.class);
        startActivity(dsp);
    }


}
