package com.example.spendlist;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity {

    ListView listview;
    androidx.drawerlayout.widget.DrawerLayout navigationView;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Toolbar Section
        navigationView = findViewById(R.id.drawer_layout);

        Toolbar t2 = findViewById(R.id.toolbari);
        t2.setNavigationIcon(getApplicationContext().getDrawable(R.drawable.ic_menu_black_24dp));
        t2.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigationView.openDrawer(Gravity.LEFT);
            }
        });
        t2.setTitle(R.string.nav_items_ballina);


        // Add Button Section
        // PSE SPO BON BRE!!!
        FloatingActionButton btnAdd = (FloatingActionButton) findViewById(R.id.btn_add);
        btnAdd.bringToFront();
//        btnAdd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(MainActivity.this, AddTransaction.class);
//                startActivity(intent);
//            }
//        });
//
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "You clicked to add register transaction!", Toast.LENGTH_SHORT).show();
            }
        });





        // Listimi i materialeve ne ListView
        listview=(ListView)findViewById(R.id.listHome);

        // sa per visualizimi, se kjo duhet me ndryshu
        final String[] transactions = {"Veshëmbathje", "Fitnes", "Rroga", "Fatura nga KEK"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, transactions);
        listview.setAdapter(adapter);


        linearLayout = (LinearLayout)findViewById(R.id.MonthCard);

        linearLayout.setClickable(true);

        linearLayout = (LinearLayout)findViewById(R.id.transactionslist);

        linearLayout.setClickable(true);


    }

    public void opnListView(View view) {
        Intent intent = new Intent(MainActivity.this, MonthTransactionHistory.class);
        startActivity(intent);
    }

    public void onClick(View view) {
        Intent intent = new Intent(MainActivity.this, MonthActivity.class);
        startActivity(intent);
    }

    public void TransactionOnClick(View view) {
        Intent intent = new Intent(MainActivity.this, MonthTransactionHistory.class);
        startActivity(intent);
    }



//    public void opnAddTransaction(View view) {
//        Intent intent = new Intent(MainActivity.this, AddTransaction.class);
//        startActivity(intent);
//    }
}
