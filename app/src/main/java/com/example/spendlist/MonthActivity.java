package com.example.spendlist;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MonthActivity extends AppCompatActivity implements View.OnClickListener  {

    private CardView January;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_month);

        January = (CardView) findViewById(R.id.January);

        January.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {


        Intent in = new Intent(MonthActivity.this, MonthTransactionHistory.class);
        startActivity(in);
    }
}
