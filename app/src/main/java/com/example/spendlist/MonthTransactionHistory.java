package com.example.spendlist;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MonthTransactionHistory extends AppCompatActivity {

    private ListView mTransactionsView;
    private Spinner mSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_month_transaction_history);
        List<String> items = new ArrayList<String>();
        List<String> prices = new ArrayList<String>();
        int[] images = {R.mipmap.ic_food };
        mTransactionsView = findViewById(R.id.monthHistory);

        items.add("Food");
        items.add("drink");
        items.add("Gym");
        items.add("Salary");
        items.add("Gift");
        items.add("Market");
        items.add("Coffe");
        items.add("College payment");
        items.add("Car fix");
        items.add("shopping");
        items.add("weekend tour");

        prices.add("10$");
        prices.add("3$");
        prices.add("50$");
        prices.add("1200$");
        prices.add("200$");
        prices.add("80$");
        prices.add("2$");
        prices.add("100$");
        prices.add("80$");
        prices.add("130$");
        prices.add("420$");

        mSpinner = findViewById(R.id.SpinnerTransctions);
        String[] kategories = {"Veshëmbathje", "Ushqim", "Paga", "Fatura","Transport"};

        ArrayAdapter<String> adapterSpinner = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, kategories);
        mSpinner.setAdapter(adapterSpinner);

//        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, items);

        MyAdapter adapter = new MyAdapter(this, items , prices,images);
        mTransactionsView.setAdapter(adapter);




        mTransactionsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent in = new Intent(MonthTransactionHistory.this, MonthActivity.class);
                startActivity(in);
            }




        });




    }


    class MyAdapter extends ArrayAdapter<String>{
        Context mContext;
        List<String> mTitle;
        List<String> mPrice;
        int rImgs[];
        MyAdapter(Context c,List<String> title, List<String> price,int imgs[]){
            super(c,R.layout.row,R.id.Title,title);
            this.mContext = c;
            this.mTitle = title;
            this.mPrice = price;
            this.rImgs = imgs;

        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.row,parent,false);
            ImageView images = row.findViewById(R.id.img);
            TextView mytitle = row.findViewById(R.id.Title);
            TextView myAmount = row.findViewById(R.id.Amount);

            images.setImageResource(rImgs[0]);
            mytitle.setText(mTitle.get(position));
            myAmount.setText(mPrice.get(position));

            return row;
        }

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.transactions_menu, menu);
//
//        MenuItem searchItem = menu.findItem(R.id.action_search);
//        SearchView searchView = (SearchView)searchItem.getActionView();
//        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                return false;
//            }
//        });
//
//        return super.onCreateOptionsMenu(menu);
//    }
}
