package com.example.spendlist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class AddTransaction extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_transaction);

        setTitle("Regjistro Tranzaksionin");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }

    public void GoToDashbouard(View view) {

        Intent in = new Intent(AddTransaction.this, MainActivity.class);
        startActivity(in);
    }
}
