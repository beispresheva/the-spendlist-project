package com.example.spendlist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.util.Objects;

public class LogIn extends AppCompatActivity {

    ImageView img ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

    }


    public void LogInMain(View view) {
        Intent in = new Intent(LogIn.this, MainActivity.class);
        startActivity(in);
    }
}
